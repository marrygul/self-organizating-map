import numpy as np
import matplotlib.pyplot as plt

from neupy import algorithms

# dots
number_dots = 36
x_array = np.random.uniform(-1, 1, number_dots)
y_array = np.random.uniform(-1, 1, number_dots)
dots = np.array(
    [[x, y] for x, y in zip(x_array, y_array)]
)

numb_clusters = 3

sofmnet = algorithms.SOFM(
    n_inputs=2,
    n_outputs=numb_clusters,

    step=0.5,
    show_epoch=20,
    shuffle_data=True,
    verbose=True,

    learning_radius=0,
    features_grid=(numb_clusters, 1),
)

plt.style.use('ggplot')
plt.plot(dots.T[0:1, :], dots.T[1:2, :], 'ko')
plt.show()

# train Self-ordinated map
result = sofmnet.train(dots, epochs=100)

print('|> Start plotting...')
plt.xlim(-1, 1.2)   # wight of plot
plt.ylim(-1, 1.2)   # height of plt
plt.plot(dots.T[0:1, :], dots.T[1:2, :], 'ko')
plt.plot(sofmnet.weight[:1, :], sofmnet.weight[1:2, :], 'rx')
plt.show()

print('|> Testing ...')
for data in dots:
    # predicted row
    row = sofmnet.predict(np.reshape(data, (2, 1)).T)
    # cluster index
    num_center = list(row[0]).index(1)
    # x of cluster center
    x_center = sofmnet.weight[:1, num_center][0]
    # y of cluster center
    y_center = sofmnet.weight[1:2, num_center][0]
    print(row, f'Center: {(x_center, y_center)}')
